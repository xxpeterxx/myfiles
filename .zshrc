if [[ -s "${ZDOTDIR:-$HOME}/.zprezto/init.zsh" ]]; then
    setopt EXTENDED_GLOB
    source "${ZDOTDIR:-$HOME}/.zprezto/init.zsh"
fi

export PATH="~/.myfiles/bin:$PATH"

if [[ "$OSTYPE" == "cygwin" ]]; then
    echo "Current OS: Cygwin inside Windows."

    alias emx='emacs --offline'
fi

if [[ "$OSTYPE" == "darwin"* ]]; then
    echo "Current OS: Mac OS."

    # Path for MacOS
    export PATH="/usr/local/bin:$PATH"
    source ~/Dropbox/Apps/OS/Mac/env.sh
    export PATH=/usr/local/anaconda3/bin:"$PATH"
    export PATH="/Users/peter/Dropbox/Apps/bin:$PATH"

    # Integrate with iTerm2.
    zstyle ':prezto:module:tmux:iterm' integrate 'yes'

    # Alias Emacs
    alias emx='open -a /usr/local/Cellar/emacs-plus/25.2/bin/emacs $1'
fi

# Linux Only
if [[ "$OSTYPE" == "linux-gnu" ]]; then
    echo "Current OS: Linux"
fi
