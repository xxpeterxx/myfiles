import numpy as np
import pandas as pd

# Q1 Part b

Mu = np.array([0.03, 0.07, 0.15, 0.20])
Sigma = np.array([0.07, 0.30, 0.25, 0.31])
Rho = np.matrix(([1, 0.4, 0.6, 0.3],
                 [0.4, 1, 0.6, 0.4],
                 [0.6, 0.6, 1, 0.4],
                 [0.3, 0.4, 0.4, 1]))
Z = np.matrix([[0.03, 1],
               [0.07, 1],
               [0.15, 1],
               [0.20, 1]])

diag_Sigma = np.diag(Sigma)
Epsilon = np.dot(np.dot(diag_Sigma, Rho), diag_Sigma)

B = np.dot(np.dot(Z.T, np.linalg.inv(Epsilon)), Z)

m_tildo = np.array([0.07, 1])
w_star = np.dot(np.dot(np.dot(np.linalg.inv(Epsilon), Z), np.linalg.inv(B)), m_tildo)
w_star = np.asarray(w_star).reshape(-1)

sigma_pi = np.sqrt(np.dot(np.dot(w_star.T, Epsilon), w_star))

print("Epsilon:\n", Epsilon)
print("B:\n", B)
print("w_star:\n", w_star)
print("sigma_pi:\n", sigma_pi)

# Q1 Part c - To find the coefficients of x**2
"""
Decision variables: Original Eq = (b12*x1 + b21*x2)**2 + (b13*x1 + b31*x3)**2 + (b14*x1 + b41*x4)**2 +
                                                         (b23*x2 + b32*x3)**2 + (b24*x2 + b42*x4)**2 +
                                                                                (b34*x3 + b43*x4)**2
"""

# Q3
# Compute tangency portfolio allocations (w_pi_star), sigma_pi, and the slope value (Sharpe) for a range of risk-free rate values 0.005, 0.0075, 0.01, ..., 0.04. Present results in a table.


def find_market_portfolio(rf, Mu, Epsilon):
    """
    Return pd.Series() containing the attributes of the market portfolio.

    Parameters
    ----------
        rf : float
            The risk-free rate
        Mu : np.array
            Array of asset returns
        Epsilon : np.matrix
            Variance-covariance matrix of asset returns
    """
    n = len(Mu)
    Ones = np.ones(n)
    numerator = np.dot(np.linalg.inv(Epsilon), Mu - np.dot(rf, Ones))
    numerator = np.asarray(numerator).reshape(-1)
    denominator = np.dot(Ones.T, numerator)
    w_pi_star = numerator / denominator
    # print("w_pi_star:\n", w_pi_star)
    sigma_pi = np.sqrt(np.dot(np.dot(w_pi_star.T, Epsilon), w_pi_star))
    sigma_pi = np.asarray(sigma_pi).reshape(-1)
    # print("sigma_pi:\n", sigma_pi)
    sharpe_pi = (np.dot(w_pi_star.T, Mu) - rf) / sigma_pi
    # print("sharpe_pi\n", sharpe_pi)
    w_t_mu = np.dot(w_pi_star.T, Mu)
    result = {'rf': rf,
              'sigma_pi': np.asscalar(sigma_pi),
              'slope': np.asscalar(sharpe_pi),
              'w_t_mu': w_t_mu}
    chars = list('ABCDEFGHIJKLMNOPQRSTUVWXYZ')
    for i in range(n):
        result['w_' + chars[i]] = w_pi_star[i]
    return(pd.Series(result))

tangent_portfolios = []
for rf in np.arange(0.005, 0.04, 0.0025):
    tangent_portfolios.append(find_market_portfolio(rf=rf, Mu=Mu, Epsilon=Epsilon))
tangent_portfolios = pd.DataFrame((tangent_portfolios))
tangent_portfolios.to_clipboard()
print(tangent_portfolios)
