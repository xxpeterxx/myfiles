#+TITLE: Pathway to Emacs
#+AUTHOR: Peter Lee
#+DATE: Aug-2017
#+STARTUP: showall


* Org Mode

** Syntax

You can make words *bold*, /italic/, _underlined_, =verbatim= and ~code~, and +strike-through+.

You can use Latex-like syntax to insert special symbols -- \alpha or \to to indicate an arrow.

Org mode supports embedding latex code into its files. When exporting to HTML, Org can use either MathJax or transcode the math into images. 

\begin{equation}
x=\sqrt{b}
\end{equation}

** Document startup visibility

Four visibility are optional, if one place the below command below a heading:
    
    #+STARTUP: overview
    #+STARTUP: content
    #+STARTUP: showall
    #+STARTUP: showeverything

** Headings

Headlines define the structure of an outline tree. The headlines in Org start with one or more stars on the left margin.
   
Hotkeys

| Key Binding | Description                      |
|-------------+----------------------------------|
| ~TAB~       | org-cycle                        |
| ~$~         | org-end-of-line                  |
| ~^~         | org-beginning-of-line            |
| ~<~         | org-metaleft                     |
| ~>~         | org-metaright                    |
| ~gh~        | outline-up-heading               |
| ~gj~        | org-forward-heading-same-level   |
| ~gk~        | org-backward-heading-same-level  |
| ~gl~        | outline-next-visible-heading     |
| ~t~         | org-todo                         |
| ~T~         | org-insert-todo-heading nil      |
| ~H~         | org-beginning-of-line            |
| ~L~         | org-end-of-line                  |
| ~o~         | always-insert-item               |
|-------------+----------------------------------|
| ~SPC m h i~ | org-insert-heading-after-current |
| ~SPC m h I~ | org-insert-heading               |
| ~SPC m h s~ | org-insert-subheading            |

** Lists

   1. Unordered list items start with -, + or * as bullets (lines must be indented or they will be seen as top-level headlines).
   2. Ordered list start with a numeral followed by either a period or a fright parenthesis such as 1. or 1) or A. or A). If you want a list of a different value, start the text of the item with [@20].
   3. Description list items are unordered list items, and contain the separator '::' to distinguish the description term from the description.

Sample

** Lord of the Rings
    My favourite senes are (in this order)
    1. The attack of the Rohirrim
    2. Eowyn's fight with the witch king
       + I really like Miranda
       + this was already my favourite
    3. Peter Jackson being shot by Legolas
       + on DVD only
       He makes a really funny face when it happens.
    But in the end, no individuals scenes matter but the film as a whole.
    Important actors in this file are:
    - Elijah Wood :: He plays Frodo
    - Sean Astin :: He plays Sam.

** Drawers
   Sometimes you want to keep information associated with an entry, but you normally don't want to see it. For this, Org mode has drawers. They can contain anything but a headline or a nested drawer.

   - Hotkey to insert a new drawer :: SPC m D

   Outside the drawer
   :DRAWERNAME:
   This is inside the drawer.
   :END:
   After the drawer.

** Footnotes

   A footnote is started by a footnote market in square brackets in column 0, no indentation allowed. The footnote reference is simply the marker in square brackets, inside text. Markers always start with fn:
   For example:
   The Org homepage[fn:1] now looks a lot better than it used to.


   Org mode extends the number-based syntax to named footnotes and optional inline definition. Here are the valie references:
   [fn:name]
             A named footnote reference.
   [fn::This is the inline definition of this footnote]
             A Latex-like anonymous footnote where the definition is given directly at the reference point.
   [fn:name:a definitino]
             An inline definitino of a footnote, which also specifies a name for the note. 
   
[fn:1] The link is: http://orgmode.org
[fn:name] The link is: http://google.com


** Tables

Org comes with a fast and intuitive table editor with spreadsheet-like calculations supported using the Emacs *calc* package.

** The built-in table editor
   Any line with '|' as the first non-whitespace character is considered part of a table. 

| Name  | Phone          | Age |
|-------+----------------+-----|
| Peter | (201) 674-3931 |  17 |
| Anna  | 234            |  23 |

   - A table is re-aligned automatically each time you press TAB inside the table;
   - Any line starting with '|-' is considered as a horizontal separator line and will be expanded on the next re-align
   - 

* Hyperlinks

** Link format
   Org will recognize plain URL-like links andactivate them as clickable links.
The general format, however, looks like this:

   [ [ link ] description] ]

** Internal links
If the link does not look like a URL, it is considered to be internal in the current file. The most important case is a linke like [ [ #my-custom-id ] ] which will link to the entry with the CUSTOM_ID property. You are responsible yourself to make sure these custom IDs are unique in a file.

Example
Hey go to this [[#my_link]]

Links such as '[ [My target] ]' or [ [My target] [Find my target] ] lead to a text search in the current file. The llink can be followed with C-c C-o when the cursor is on the link.

Example
Hey to to this [[my_link]], which pionts to the next occurrence of < < my_link > >. and will search across the document.
yoyo this is testing <<my_link>>

Return to the previous position can be invoked with C-c &
** Search options in file links
File links can contain additional information to make Emacs jump to a particular location in the file when following a link. This can be a line number or a search option after a double colon.

For example, when the command C-c l creates a link to a file, it encodes the words in the current line as a search string that can be used to find this link back later when following the link with C-c C-o.

Here is the syntax of ways to attach a search to a file link:

[ [ file: ~/.myfiles/docs/emacs.org::255 ] ]
[[file:~/.myfiles/docs/emacs.org::255]]
[ [ file: ~/.myfiles/docs/emacs.org::/regexp/] ]
[ [ file: ~/.myfiles/docs/emacs.org::*My Target] ]

255: Jump to line 255
/regexp/: Do a regular expression search for regexp.
*My Target: In an Org file, restrict search to headlines.

* TODO items
With Org mode, simply mark any entry in a tree as being a TODO item. In this way, information is not duplicated, and the entire context from which the TODO item emerged is always present. 

Of course, this technique for managing TODO items scatters them throughout your notes file. Org mode compensates for this by providing methods to give you an overview of all the things that you have to do.

** Basic TODO functionality
Any headlines becomes a TODO item when it starts with the word 'TODO'.

- t :: rotate the TODO state of the heading
- [ ] T :: Insert todo heading
  - [ ]  M-t :: Insert todo heading + org-metaright

testing.  
+ testing
+ testing2
+ [ ] Inserted todo headings
  + [ ] Insert todo headings + org-metaright

** TODO dependencies

Usually, a parent TODO task should not be marked DONE until all subtasks (defined as children tasks) are marked as DONE. 
If you customize the option org-enforce-todo-dependencies, Org will block entries from changing state to DONE while they have children that are not DONE.

** Priorities
Prioritizing can be done by placing a priority cookie into the headline of a TODO item, like this
*** TODO [#A] A prioritized todo item.

** Show percentage of completion
To keep the overview over the fraction of subtasks that are already completed, insert either '[/]' or '[%]' anywhere in the headline. These cookies will be updated each time the TODO status of a child changes, or when pressing C-c C-c on the cookie. 


Note that Org mdoe supports three priorities: A, B and C. A is the highest priority. An entry without a cookie is treated just like priority 'B'.

Priorities make a difference only for sorting in the agenda. Outside the agenda, they have no inherent meaning to Org mode.
* TODO Blocked until (two) is done [66%]
** TODO one
** DONE two
   CLOSED: [2017-07-31 Mon 08:40]
** DONE three
   CLOSED: [2017-07-31 Mon 08:40]

* Parent
  :PROPERTIES:
  :ORDERED: t
  :END:
** TODO a
** TODO b, needs to wait for (a)
** TODO cc, needs tro wait for (a) and (b)

** Checkboxes
Every item in a plain list can be made into a checkbox by starting it with the string '[ ]'. This feature is similar to TODO items, but is more lightweight. 

Checkboxes are not included in the global TODO list, so they are often great to split a task into a number of simple steps.

To tick a checkbox, use C-c C-c.

* TODO Organize party [2/5]
  - [-] call people [1/3]
    - [ ] Peter
    - [X] Sarah
    - [ ] Sam
  - [X] order food
  - [ ] think about music
  - [ ] insert a new item
  - [X] talk to the neighbours

Key bindings:
M-S-RET : Insert a new item with a checkbox. 

* Tags :work:urgent:
An excellent way to implement labels and contexts for cross-correlating information is to assign tags to headlines.

Every headline can contain a list of tags; they occur at the end of the headline. 

Tags must be preceded and followed by a single colon, e.g., ':work:urgent:'

You may specify special faces for specific tags using the option org-tag-faces in much the same way as you can for TODO keywords.

** Tag inheritance

Tags make use of the hierarchical structure of outline trees. If a heading ahs a certain tag, all subheadings will inherit the tag as well.

** Setting tags

Tags can simply be typed into the buffer at the end of a headline. After a colon, M-TAB offers completion on tags. 

* Properties and columns
A property is a key-value pair associated with an entry. Properties can be set so they are associated with a single entry, with every entry in a tree or with every entry in an Org mode file. 

There are two main applications for properties in Org mode. 

1) Properties are like tags, but with a value. Imagine maintaining a file where you document bugs and plan releases for a piece of sogtware, instead of using tags like :release_1:, :release_2:, you can use a property, say :Release:, that in difference usbtrees has differet values, such as 1.0 and 2.0.
2) You can use properties to implement (very basic) database capabilities in an Org buffer. Imagine keeping track of your music CDs, where properties could be things such as the albumn, artist, date of release and so on.

Properties can b econveniently edited and viewed in column view.

** Property syntax
Properties are key-value pairs. They need to be inserted into a special drawer with the name PROPERTIES, and has to be located right below a headline and its planning line when applicable. Keys are case-insensitive.

Here is an example:

* CD collection
** Classic
     :PROPERTIES:
     :GENRES:     Classic
     :END:
*** Goldberg Variations
     :PROPERTIES:
     :Title:      Goldberg Variations
     :Composer:   J.S. Bach
     :Artist:     Glen Gould
     :Publisher:  Duetsche Grammophon
     :NDisks:     1
     :GENRES+:    Baroque
     :END:

The above results in the generes property having the value "Classic Baroque" under the Goldberg Variations subtree.

key bindings
- SPC m P :: org-set-property

** Column view
In column view, each outline node is turned into a table row. Columns in this table provide access to perperties of the entries. 

Org mode implements columns by overlaying a tabular structure over the headline of each item.

Setting up a column view first requires defining the columns. This is done by defining a column format line.

To define a column format for an entire file, use a line like

#+COLUMNS: %25ITEM %TAGS %PRIORITY %TODO

To specify a format that only applies to a specific tree, add a :COLUMNS: property to the top node of that tree, for example:

** Top node for columns view
    :PROPERTIES:
    :COLUMNS: %25ITEM %TAGS %PRIORITY %TODO
    :END:

** Column attributes
A column definitions sets the attributes of a column. The general definition looks like this:
    %[width]property[(title)][{summary-type}]
All items are optional.

- width :: an integer specifying the width of the column in characters. If ommited, width is determined automatically.
- property :: the property that should be edited in this column.
- title :: the header text for the column.
- {summary-type} :: if specified, the column values for parent nodes are computed from the children.
  + {+} :: sum numbers in this column
  + {min} :: smallest number in column 
  + {est+} :: add 'low-high' estimates
  + {X} :: checkbox status, '[X]' if all children are '[X]' 
etc.

The est+ summary type requires further explanation. It is used for combinaing estiamtes, expressed as "low-high" ranges. For example, instead of estimating a particular task will take 5 days, you might estiamte it as 5-6 days. When combining a set of such estimates, simply adding the lows and highs produce an unrealistically wide result. Instead, est+ adds the statistical mean and variance of the sub-teasks, generating a final estiamte from the sum. 


* Dates and times

TODO items cna be labeled with a date and/or a time. The specially formatted string carrying the date and time is called a timestamp.

** Timestamps, deadlines and scheduling

Format:
- <2003-09-16 Tue>
- <2003-09-16 Tue 09:38>
- <2003-09-16 Tue 09:38-10:00>

- Plain timestamp; Event; Appointment :: A simple timestamp just assigns a date/time to an item.
* Meet Peter at the moveis
<2003-09-16 Tue 09:38>

- Timestamp with repeater interval :: a timestamp may contain a repeater interval, indicating that it applies not only on the given date, but again and again after a certain interval of N days (d), weeks (w), months (m) or years (y). The following shall show up in the agenda every Wednesday.
* Pick up Kayden at school
<2007-05-16 Wed 12:30 +1w>

- Time/Date range :: two timestamps connected by "--" dentoe a range. The headline will be shown on the first and last day of the range, and on any dates that are displayed and fall in teh range.
** Meeting in Amsterdam
<2004-08-23 Mon>--<2004-08-26 Thu>

- Inactive timestamp :: just linke a plain timestampe, but with square brackets and they do not trigger an entry in agenda.
* Inactivte item
[2017-11-11 Sat]

For Org mode to recognize timestamps, they need to be in the specific format.

Keybindings
- C-c . prompt for a date and insert a corresponding timestamp <2017-08-13 Sun>.

** Deadlines and scheduling
A timestamp may be preceded by special keywords to facilitate planning. Both the time-stamp and the keyword have to be positioned immediately after the task they refer to.

- DEADLINE :: the task is upposed to be finished on that date. On the deadline date, the task will be listed in the agenda. In addition, the agenda for today will carry a warning about the approaching or missed deadline, starting org-deadline-warning-days before the due date.
 
*** TODO write article about the Earth
     DEADLINE: <2017-08-31>
     The editor in charge is [[bbbdb:Ford Prefect]]

You can specify a different lead time for warnings for a specific deadline using the following syntax.
 
*** TODO write article about the Earth 2
     DEADLINE: <2017-08-31 -5d>
     The editor in charge is [[bbbdb:Ford Prefect]]

- SCHEDULED :: you are planning to start working on that task on the given date. The headline will be listed under the given date. In addition, a reminder that the scheduled date has passed will be present in the compilation for today, until the entry is marked DONE, i.e., the task will automatically be forwarded until completed.

*** TODO Call Trillian for a date on New Years Eve.
     SCHEDULED: <2017-12-01>

Important: Scheduling an item in Org mode should not be understood in the same way that we understand scheduling a meeting. Setting a date for a meeting is just a simple appointment, you should market this entry with a simple plain timestamp. In Org mode, scheduling means setting a date when you want to start working on an action item.

Keybindings:
- C-c C-d :: Insert 'DEADLINE' keyword along wth a stamp.
- C-c C-s :: Insert 'SCHEDULED' keyword along with a stamp.

Deadlines and scheduled items produce entries in the agenda when they are over-due, so it is important to be able to mark such an entry as completed once you have done so. 
When you try to mark such an entry DONE (using C-c C-t), it will shift the base date of the repeating timestamp by the repeater interval, and immediately set the enty state back to TODO.
To mark a task with a repeater as DONE, use C-- 1 C-c C-t.

** Clocking work time 
Org mode allows you to clock the time you spend on specific tasks in a project.

Key bindings
- C-c C-x C-i :: Start the clock on the current item (clock in). This inserts the clock keyword together with a timestamp.
- C-c C-x C-o :: Stop the clock. This inserts another timestamp at the same location where the clock was last started.

* Capture - Refile - Archive

An important part of any organization system is the ability to quickly capture new ideas and tasks, and to associate reference material with them. Once in the system, tasks and projects need to be moved around. Moving completed project trees to an archive file keeps the system compact and fast.

** Capture
Capture lets you quickly store ntoes with little interruption of your work flow. 

*** Setting up capture
The following customization sets a default target file for notes, and defines a global key for capturing new material.

(setq org-default-notes-file (concat org-directory "/notes/org"))
(define-key global-map "\C-cc" 'org-capture)

*** Using capture
Keybindings
- SPC c :: Call the command org-capture
- C-c C-c :: Once you ahve finished entering information into the capture buffer, this will return you to the window configuration before the capture process, so that you can resume your work without further distraction. 
- C-c C-w :: Finalize the capture process by refiling the note to a different place. The cursor location will determine which part of the tree to be refiled.
- C-c C-k :: Abort the capture process.

** Attachments
Hyperlinks can establish associations with files that live elsewhere, like emails or source code files.
Attachments are files located in a directory belonging to an outline node. Org uses directories named by the unique ID of each entry. These directories are located in the data directory which lives in the same directory where your Org file lives. 

- C-c C-a :: The dispatcher for commands related to the attachment system. A list of commands is displayed and you must press an additional key to select a command
             + a :: select a file and move it into the task's attachment directory.
             + c/m/l :: copy/move/link method to attach a file.
             + f :: open the current task's attachment directory

** Refile and copy
When reviewing the captured data, you may want to refile or to copy some of the entries into a different list, for example, into a project. Cutting, finding the right location, and then pasting the note is cumbersome. To simplify this process:

- C-c M-w :: copying works like refiling, except that the original note is not deleted.
- C-c C-w :: refile the entry or region at point. This command offers possible locations for refiling the entry. 

** Archiving
When a project represeted by a tree is finished, you may want to move the tree out of the way and to stop it from contributing to the agenda. Archiving is important to keep your working files compact and global searches like the construction of agenda views fast.l

- C-c C-x C-a :: archive the current entry using the command specified in the variable org-archive-default-command.

*** Moving a tree to the archive file
The most common archiving action is to move a project tree to another file, the archive file.

C-c C-x C-s or short C-c $ :: Archive the subtree starting at the cursor position to the location given by org-archive-location.

The default archive location is a file in the same directory as the current file, with the name derived by appending _archive to the current file name. You can also choose what heading to file archived items under, with the possibility to add them to a datetree ina file. 

* Agenda views
Due to the way Org works, TODO items, time-stamped items and tagged headlines can be scattered throughout a file or even a number of files. To get an overview of open action items, or of events that are important for a particular date, this information must be collected, sorted and displayed in an organized way.

Several different view types are provided:
- Agenda :: it is like a calendar and shows information for specific dates
- TODO list :: covers all unfinihsed action items.
- Match view :: showing headlines based on the tags, properties and TODO state
- Timeline view :: shows all events in a single Org file, in time-sorted view
- Text search view :: shows all entries from multiple files that contain specified keywords
- stuck projects view :: shows projects that currently don't move along
- custom views :: special searches and combinations of views

** Agenda files
The information in agendar is normally collected from all aagenda files, the files listed on the variable org-agenda-files.Thus, even if you only work with a single Org file, that file should be put into the list. You can sutomize org-agenda-files, but the easiest way to maintain it is through the following commands

- C-c [ :: Add current file to the list of agenda files. The file is added to the font of the list.
- C-c ] :: Remove current file from the list of agenda files.
- C-' :: Cycle through agenda file list 
- C-, :: Cycle through agenda file list

** The agenda dispatcher 
- C-c a :: global key, followed by:
  - a :: create teh calendar-like agenda
  - t / T :: create a list of all TODO items
  - m / M :: craete a list of headlines matching a TAGS expression
  - # / I :: create a list of stuck projects 

             
