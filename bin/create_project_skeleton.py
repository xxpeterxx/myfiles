"""
Project: Generate the skeleton folder/file structure for a new Python project.
Author: Peter Lee (mr.peter.lee@hotmail.com)
Date: Aug-17-2017
Version: 0.1 Gamma

This script will create the basic mock files and folders suggsted for a new project, so that the end user could easily start coding for the project. 

- Name the top level folder carefully, i.e., ~Apple~, and when you do releases, you should include a version number suffix like =Apple-2.5=.
- Create a directory ~Apple/bin~ and put executables there. Given them ~.py~ since Windows requires them. Don't put any code in them except an import of and call to a main function defined somewhere else in your projects.
- If your project is expressable as a single Python source file, then put it into the directory and name it something related to your project, i.e., ~Apple/apple.py~. If you need multiple source files, create a package instead (~Apple/apple/~ with an empty ~Apple/apple/__init__.py~) and place your source files in it. For example, ~Apple/apple/internet.py~.
- Put your unit tests in a sub-package of your package - always need at least one other file for your unit tests. For example, ~Apple/apple/test/~. Of course, make it a package with ~Apple/apple/test/__init__.py~, and place tests in files like ~Apple/apple/test/test_internet.py~.
- Add ~Apple/README.md~ and ~Apple/setup.py~ to explain and install your software, if you're feeling nice.
- Don't put your source in a directory called ~src~ or ~lib~. This makes it hard to run without installing.
- Don't put your tests outside of your Python package, as it makes it hard to run the tests against an installed version.
- Don't try to come up with magical hacks to make Python able to import your module or package without having the user add the directory containing it to their import path (via PYTHONPATH).
  
File structure - Project Apple
\- bin/
\   |-- apple.py
\- docs/
\   |-- conf.py
\   |-- apple.md
\   |-- index.md
\- apple/
\   |-- test/
\       |-- __init__.py
\       |-- test_model.py
\   |-- __init__.py
\   |-- model.py
\   |-- main.py
\- LICENSE
\- README.md
\- TODO.md
\- setup.py
\- requirements.txt

"""


import pathlib
import argparse
import pandas as pd


def create_blank_target(path, target, is_file=True):
    """
    Creates a blank file.

    Parameters
    ----------
        path : pathlib.Path()
            The path of the folder.
        filename : str
            The name of the blank file.
        is_file : Boolean
            True to create a file, False to create a folder.
    """
    if not pathlib.Path(path, target).exists():
        if is_file:
            pathlib.Path(path, target).touch()
        else:
            pathlib.Path(path, target).mkdir()
    print("Created blank folder/file: {}".format(pathlib.Path(path, target)))

def main(project_name):
    """
    Parameters
    ----------
        project_name : str
            The name of the project
    """
    project_name = project_name.lower()
    main_folder = pathlib.Path(".", project_name.title())
   
    # Create the folders
    if not pathlib.Path(main_folder).exists():
        pathlib.Path(main_folder).mkdir()

    folders = ['bin', 'docs', project_name, '{}/test'.format(project_name)]
    for target in folders:
        create_blank_target(main_folder, target, is_file=False)
    
    # Create the files
    files = ['bin/{}.py'.format(project_name),
             '{}/__init__.py'.format(project_name),
             '{}/main.py'.format(project_name),
             '{}/test/__init__.py'.format(project_name),
             'LICENSE',
             'README.md',
             'setup.py',
             'requirements.txt'
             ]
    
    for target in files:
        create_blank_target(main_folder, target, is_file=True)

    # Update content for project_name\main.py
    main_project_main_py(project_name)

def main_project_main_py(project_name):
    """
    Prefil the content for file \project_name\main.py
    """
    content = """\"\"\"
Project: {project_name}
Author: Peter Lee
Date: {date}

Description
-----------

\"\"\"

import argparse


def main(args):
    \"\"\"
    Main program.

    Parameters
    ----------
        args : argparse.ArgumentParser.parse_args()
            The program arguments.
    "\"\"

    if args.verbose:
        print("verbose is on, let's do something verbosily!")
    elif args.quiet:
        print("quiet is on!")

    print("{project_name} completes successfully.")


if __name__ == "__main__":
    # Parse program arguments
    arguments_parser = argparse.ArgumentParser(description="Please specify the description of the program.")
    arguments_exclusive_groups = arguments_parser.add_mutually_exclusive_group()
    arguments_exclusive_groups.add_argument("-v", "--verbose", help="Display more details in program output", action="store_true")
    arguments_exclusive_groups.add_argument("-q", "--quiet", help="Display only core details in program output", action="store_true")
    arguments_parser.add_argument("-n", "--number", help="this is an example of categorical argument to be parsed into integers", type=int, default=0, choices=[0,1,2,3])
    args = arguments_parser.parse_args()

    main(args)
    """.format(project_name=project_name,
               date=pd.datetime.now().strftime("%B-%d-%Y"))

    #.format(date=pd.datetime.now().strftime("%B-%d-%Y"))
    pathlib.Path(project_name, project_name, 'main.py').write_text(content)

if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument('project_name')
    args = parser.parse_args()
    main(args.project_name)
