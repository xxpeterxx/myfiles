# The deployment process is separated into disctivtive sessions for Windows/Mac/Linux.

if [[ "$OSTYPE" == "cygwin" ]]; then
    # Cygwin emulation for Windows
    echo "Current OS: Cygwin inside Windows."

    echo "Pull from the lastest and merge with local changes"
    cd ~/.myfiles
    # git reset --hard origin/master
    git pull origin master

    echo "now copy and overwrite dot files in the HOME directory"
    cp ~/.myfiles/.* ~/

    echo "on specific hosts: over write dot folders in the HOME directory with the latest"
    if [[ "$(hostname)" == "USJAXDT988994" ]]; then
        cp -r ~/.myfiles/vim/.vim ~
        cp -r ~/.myfiles/vim/.vim_runtime ~
        cp -r ~/.myfiles/zsh/.zprezto ~
        cp -r ~/.myfiles/emacs/.emacs.d ~
    fi
fi

if [[ "$OSTYPE" == "darwin"* ]]; then
    echo "Current OS: Mac OS."

    echo "will pull from the lastest and MERGE with local changes"
    cd ~/.myfiles
    git pull origin master

    # This below locates all dot files / folders within current path and re-create symbolic links to the home folder.
    # Get the current path
    SRC="$(pwd)"

    for file_path in $(find $SRC -name ".*" -maxdepth 1)
    do
        file_name=$(basename "${file_path}")

        # Exclude files/folders
        if [[ (${file_name} = ".myfiles") || ( ${file_name} = '.gitignore' ) || ( ${file_name} = '.git' ) ]]
        then
            continue
        fi

        # Create symlinks for dot files
        if [ -e $HOME/${file_name} ]
        then
            echo "File ${file_name} exists in the home directory."
            # read -r -p "Do you want to DELETE ${HOME}/${file_name}? [Y/n]" response

            # if [[ $response =~ ^(yes|y|Y| ) ]]
            # then
                rm $HOME/${file_name}
                echo "Deleted ~/${file_name}"
                ln -s ${file_path} $HOME/${file_name}
                echo "Created a new symbolic link referecing to ${file_path}."
            # fi
        else
            ln -s ${file_path} $HOME/${file_name}
            echo "File ~/${file_name} does NOT exist. Created a new symbolic link referecing to ${file_path}."
        fi
    done
fi

if [[ "$OSTYPE" == "linux-gnu" ]]; then
    echo "Current OS: Linux"

    echo "will pull from the lastest and MERGE with local changes"
    cd ~/.myfiles
    git pull origin master
fi

