# The deployment process is separated into disctivtive sessions for Windows/Mac/Linux.
# -m "commit comments"

# Getting Git Comments from the user
  # A POSIX variable
  OPTIND=1         # Reset in case getopts has been used previously in the shell.

  # Initialize my own variables:
  comment=""
  verbose=0

  while getopts "h?vm:" opt; do
      # Please note m: means -m is required
      # v means -v is not always required
      case "$opt" in
          h|\?)
              show_help
              exit 0
              ;;
          v)  verbose=1
              ;;
          m)  comment=$OPTARG
              ;;
      esac
  done

  shift $((OPTIND-1))

  [ "$1" = "--" ] && shift

  # echo "verbose=$verbose, comment='$comment', Leftovers: $@"


# Windows Cygwin
if [[ "$OSTYPE" == "cygwin" ]]; then

    # Cygwin emulation for Windows
    echo "Idendified current OS is Cygwin inside Windows."

    # Get the current path
    SRC="${HOME}/.myfiles"

# Disabled as the below doesn't seem to be working
#    # For each files in .myfiles path, replace it by the file from HOME of the same name
#    for file_path in $(find ${SRC} -maxdepth 1 -name ".*")
#    do
#        file_name=$(basename "${file_path}")
#
#        # Exclude files/folders
#        if [[ (${file_name} == ".myfiles") || ( ${file_name} == '.gitignore' ) || ( ${file_name} == '.git' ) ]]
#        then
#            continue
#        fi
#
#        # Locate the identical file in HOME
#        if [ -e $HOME/${file_name} ]
#        then
#            echo "Located ${file_name} in the home directory."
#            # read -r -p "Do you want to replace ${file_path} with ${HOME}/${file_name}? [Y/n]" response
#
#            # if [[ $response =~ ^(yes|y|Y| ) ]]
#            # then
#                cp $HOME/${file_name} ${file_path}
#                echo "${file_name} has successfully been updated."
#            # fi
#        else
#            echo "File ${file_name} does NOT exist in ${HOME}, this file will not be updated."
#        fi
#    done

    cp ~/.vimrc ~/.myfiles/.vimrc
    cp ~/.spacemacs ~/.myfiles/.spacemacs
    cp ~/.zpreztorc ~/.myfiles/.zpreztorc
    cp ~/.zshrc ~/.myfiles/.zshrc
    cp ~/.tmux.conf ~/.myfiles/.tmux.conf

    # Some specific folders - only update these folders excluding specific hosts
    # if [[ "$(hostname)" != "USJAXDT988994" ]]; then
    echo "removing .vim folder"
        rm -rf ~/.myfiles/vim/.vim
        cp -r ~/.vim ~/.myfiles/vim/
        find ~/.myfiles/vim/.vim -name "*.git" -exec rm -rf {} \;
        find ~/.myfiles/vim/.vim -name "*.gitignore" -exec rm -rf {} \;
        find ~/.myfiles/vim/.vim -name "*.github" -exec rm -rf {} \;

    echo "removing .vim_runtime folder"
        rm -rf ~/.myfiles/vim/.vim_runtime
        cp -r ~/.vim_runtime ~/.myfiles/vim/
        find ~/.myfiles/vim/.vim_runtime -name "*.git" -exec rm -rf {} \;
        find ~/.myfiles/vim/.vim_runtime -name "*.gitignore" -exec rm -rf {} \;
        find ~/.myfiles/vim/.vim_runtime -name "*.github" -exec rm -rf {} \;

    echo "removing .zprezto folder"
        rm -rf ~/.myfiles/zsh/.zprezto
        cp -r ~/.zprezto ~/.myfiles/zsh/
        find ~/.myfiles/zsh/.zprezto -name "*.git" -exec rm -rf {} \;
        find ~/.myfiles/zsh/.zprezto -name "*.gitignore" -exec rm -rf {} \;
        find ~/.myfiles/zsh/.zprezto -name "*.github" -exec rm -rf {} \;

    echo "removing .emacs.d folder"
        rm -rf ~/.myfiles/emacs/.emacs.d
        cp -r ~/.emacs.d ~/.myfiles/emacs/
        find ~/.myfiles/emacs/.emacs.d -name "*.git" -exec rm -rf {} \;
        find ~/.myfiles/emacs/.emacs.d -name "*.gitignore" -exec rm -rf {} \;
        find ~/.myfiles/emacs/.emacs.d -name "*.github" -exec rm -rf {} \;
    # fi

    # Git push
    git add .
    git commit -m "${comment}"
    git push origin master
fi



# Mac OS
if [[ "$OSTYPE" == "darwin"* ]]; then
    echo "Idendified OS: Mac OS."

    # Some specific folders
    rm -rf ~/.myfiles/vim/.vim
    cp -r ~/.vim ~/.myfiles/vim/
    rm -rf ~/.myfiles/zsh/.zprezto
    cp -r ~/.zprezto ~/.myfiles/zsh/

    git add .
    git commit -m "${comment}"
    git push origin master
fi


