;;; -*- no-byte-compile: t -*-
(define-package "scad-mode" "20170219.2003" "A major mode for editing OpenSCAD code" 'nil :commit "959b9c4b712ed5db6938fc6ebdf2c1cb58df6c4f" :url "https://raw.github.com/openscad/openscad/master/contrib/scad-mode.el" :keywords '("languages"))
