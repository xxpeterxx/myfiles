;;; -*- no-byte-compile: t -*-
(define-package "esh-help" "20170730.2157" "Add some help functions and support for Eshell" '((dash "1.4.0")) :commit "fdd62fb873e2a9a664314a45f91529cac00b8c47" :url "https://github.com/tom-tan/esh-help/" :keywords '("eshell" "extensions"))
