;;; -*- no-byte-compile: t -*-
(define-package "info+" "20170804.1530" "Extensions to `info.el'." 'nil :url "https://www.emacswiki.org/emacs/download/info%2b.el" :keywords '("help" "docs" "internal"))
