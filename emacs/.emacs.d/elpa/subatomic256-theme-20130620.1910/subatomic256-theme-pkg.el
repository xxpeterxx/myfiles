;;; -*- no-byte-compile: t -*-
(define-package "subatomic256-theme" "20130620.1910" "Fork of subatomic-theme for terminals." 'nil :commit "326177d6f99cd2b1d30df695e67ee3bc441cd96f" :url "https://github.com/cryon/subatomic256")
