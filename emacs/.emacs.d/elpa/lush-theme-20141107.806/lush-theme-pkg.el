;;; -*- no-byte-compile: t -*-
(define-package "lush-theme" "20141107.806" "A dark theme with strong colors" '((emacs "24")) :commit "3b80004f33cdce9f4db69e9ccf2041561e98985d" :url "https://github.com/andre-richter/emacs-lush-theme" :keywords '("theme" "dark" "strong colors"))
