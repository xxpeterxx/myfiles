;;; -*- no-byte-compile: t -*-
(define-package "seti-theme" "20161208.836" "A dark colored theme, inspired by Seti Atom Theme" 'nil :commit "cbfef2fc15d19ce4c8326e65fafdd61737077132" :url "https://github.com/caisah/seti-theme" :keywords '("themes"))
