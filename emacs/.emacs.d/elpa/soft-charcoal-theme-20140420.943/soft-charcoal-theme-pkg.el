;;; -*- no-byte-compile: t -*-
(define-package "soft-charcoal-theme" "20140420.943" "Dark charcoal theme with soft colors" 'nil :commit "5607ab977fae6638e78b1495e02da8955c9ba19f" :url "http://github.com/mswift42/soft-charcoal-theme")
