;;; -*- no-byte-compile: t -*-
(define-package "eshell-prompt-extras" "20170713.11" "Display extra information for your eshell prompt." 'nil :commit "52a8ce66da2c5f419efd41f2a6b6d4af03f78acd" :keywords '("eshell" "prompt"))
