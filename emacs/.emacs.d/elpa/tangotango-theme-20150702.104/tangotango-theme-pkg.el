;;; -*- no-byte-compile: t -*-
(define-package "tangotango-theme" "20150702.104" "Tango Palette color theme for Emacs 24." 'nil :commit "08c3b9270547970dbce2cb1e35e66f6ae380c8b2" :url "https://github.com/juba/color-theme-tangotango" :keywords '("tango" "palette" "color" "theme" "emacs"))
