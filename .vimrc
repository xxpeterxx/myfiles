set nocompatible

" Try to load Ultimate VIMRC if it is installed
try
  set runtimepath+=~/.vim_runtime
  source ~/.vim_runtime/vimrcs/basic.vim
  source ~/.vim_runtime/vimrcs/filetypes.vim
  source ~/.vim_runtime/vimrcs/plugins_config.vim
  source ~/.vim_runtime/vimrcs/extended.vim
catch
endtry

" Force Python3 on dynamic interp built Vim
if has("python3")
   command! -nargs=1 Py py3 <args>
else
  command! -nargs=1 Py py <args>
endif

" ==================== General Config ==================== "
set clipboard=unnamed                    " Use system clipboard
set number                               " Line numbers are good
set backspace=indent,eol,start           " Allow backspace in insert mode
set history=1000                         " Store lots of :cmdline history
set showcmd                              " Show incomplete cmds down the bottom
set showmode                             " Show current mode down the bottom
set gcr=a:blinkon0                       " Disable cursor blink
set visualbell                           " No sounds
set noerrorbells
set autoread                             " Reload files changed outside vim
set guifont=SourceCodePro-Regular\ 11    "Change default font
set mouse=a                              " Activate mouse
set selectmode+=mouse                    " Mouse on select mode
set autochdir                            " Automatically change window's cwd to file's dir
set textwidth=0                          " Lines longer than 79 columns will be broken. This setting is overriden in g:python_normal_text_width
set formatoptions=qrn1
set colorcolumn=0                        " No more annoying colored column to indicate page width
set modelines=0                          " Prevent some securitiy exploits
set exrc                                 " exrc option. This option forces Vim to source .vimrc file if it present in working folder
set secure
set hidden                               " This makes vim act like all other editors, buffers can exist in the background without being in a window. 
inoremap fd <ESC>                        " Remap f d to <ESC>

" Color Scheme - disabled due to using Ultiamte VIMRC
set background=dark             "Solarized Theme
set t_Co=256
if &t_Co >= 256 || has("gui_running")
    "colorscheme wombat256mod
    "colorscheme peter
    "colorscheme pablo
endif

if &t_Co > 2 || has("gui_running")
    " switch syntax highlighting on, when the terminal has colors
    syntax on
endif

" more subtle popup colors 
if has ('gui_running')
   highlight Pmenu guibg=#cccccc gui=bold    
endif

" Change leader to a comma (if need to change to SPACE, set it to ''\ '') 
let mapleader=","

" To paste in a large text while preserving the original formatting.
" When in insert mode, ready to paste, switch to paste mode first and paste.
" Then switch off the paste mode.
set pastetoggle=<Leader>p
" to use <C-r>+ to paste right from the OS paste board. Of course, this only works when running Vim locally (i.e. not over an SSH connection).

" Formatting the current paragraph (or selection)
vmap Q gq
nmap Q gqap

" Long lines with line wrapping enabled - Jump the cursor 'over' the current line to the next line.
nnoremap j gj
nnoremap k gk

" Clears the search buffer when you press <SPACE>/ (clearing highlighted searches)
nmap <silent> \ / :nohlsearch<CR>

" Forgot to use sudo before editing a file that requires root privileges. This lets you use w!! to do that after you opened the file already.
cmap w!! w !sudo tee % >/dev/null

" Save on losing focus
au FocusLost * :wa

" Tell vim to remember certain things when we exit
"  '10  :  marks will be remembered for up to 10 previously edited files
"  "100 :  will save up to 100 lines for each register
"  :20  :  up to 20 lines of command-line history will be remembered
"  %    :  saves and restores the buffer list
"  n... :  where to save the viminfo files
set viminfo='10,\"100,:20,%,n~/.viminfo
function! ResCur()
  if line("'\"") <= line("$")
    normal! g`"
    return 1
  endif
endfunction

augroup resCur
  autocmd!
  autocmd BufWinEnter * call ResCur()
augroup END

" Auto save when focus is lost
au FocusLost * silent! wa
set autowriteall


" =============== Vundle Initialization ===============
" set the runtime path to include Vundle and initialize
set rtp+=~/.vim/bundle/Vundle.vim
call vundle#begin()
" alternatively, pass a path where Vundle should install plugins
"call vundle#begin('~/some/path/here')

" let Vundle manage Vundle, required
Plugin 'gmarik/Vundle.vim'                " To auto manage Vim packages
" Plugin 'klen/python-mode'
Plugin 'scrooloose/nerdtree'              " A file browser
Plugin 'ConradIrwin/vim-bracketed-paste'
Plugin 'godlygeek/tabular'
Plugin 'plasticboy/vim-markdown'
Plugin 'christoomey/vim-tmux-navigator'
" Plugin 'flazz/vim-colorschemes'
Plugin 'chriskempson/base16-vim'          " Color scheme
Plugin 'benmills/vimux'
Plugin 'julienr/vim-cellmode'
Plugin 'jgdavey/tslime.vim'               " Send stuff to nearest Tmux pane
Plugin 'jmcantrell/vim-virtualenv'         " Support for python virtualenv
"Plugin 'jpalardy/vim-slime'               " Send stuff to nearest Tmux pane
"
Plugin 'davidhalter/jedi-vim'             " Python autocompletion library
Plugin 'ervandew/supertab'                " Tab completion in Python coding
"Plugin 'kien/rainbow_parentheses.vim'     " Rainbow Parentheses (toggled to <Leader>R)
"Plugin 'scrooloose/syntastic'             " Extensive syntax checkers (must install supported checkers from https://github.com/scrooloose/syntastic/wiki/Syntax-Checkers)
" Plugin 'mrpeterlee/VimWordpress'              " Vim for Wordpress

"Plugin 'Valloric/YouCompleteMe' 
Plugin 'vim-scripts/CycleColor'            " Press F4 to cycle available color themes
Plugin 'flazz/vim-colorschemes'

" All of your Plugins must be added before the following line
call vundle#end()            " required
filetype plugin indent on    " required
filetype plugin on           " required

" Brief help
" :PluginList       - lists configured plugins
" :PluginInstall    - installs plugins; append `!` to update or just
" :PluginUpdate
" :PluginSearch foo - searches for foo; append `!` to refresh local cache
" :PluginClean      - confirms removal of unused plugins; append `!` to
" auto-approve removal

" see :h vundle for more details or wiki for FAQ
" Put your non-Plugin stuff after this line

" ================ Work with C++/C ==================
augroup project
    autocmd!
    autocmd BufRead,BufNewFile *.h,*.c set filetype=c.doxygen
augroup END
" ================ You Complete Me ==================
let g:ycm_python_binary_path = '/usr/local/bin/python3'

" ================ Backup Settings ==================
set noswapfile
set nobackup
set nowritebackup
" Create the backup folder if necessary (group previlege)
if !isdirectory($HOME.'/.vim/.backup')
    silent call mkdir ($HOME.'/.vim/.backup', 'p')
endif
set backupdir=~/.vim/.backup
set directory=~/.vim/.backup
" Delete backup files older than 120 days
silent execute '!find ~/.vim/.backup -mindepth 1 -mtime +120 -delete'

" ================ Persistent Undo ==================
" Keep undo history across sessions, by storing in file.
if has('persistent_undo')
  silent !mkdir ~/.vim/backups > /dev/null 2>&1
  set undodir=~/.vim/backups
  set undofile
endif

" ================ Indentation ======================
set autoindent                  " Align the new line indent with the previous line
" set smartindent               " Causing comments in Python to be indented
                                " incorrectly
" set cindent
set smarttab
set shiftwidth=4                " Operation >> indents 4 columns; << unindents 4 columns
set softtabstop=4               " Insert/delete 4 spaces when hitting a Tab/Backspace
set tabstop=4                   " A hard Tab displays as 4 columns
set expandtab                   " Insert spaces when hitting Tabs
set shiftround                  " round indent to multiple of 'shiftwidth'

" Display tabs and trailing spaces visually
set list
set listchars=tab:>.,trail:.,extends:#,nbsp:.    " This line will make Vim set out tab characters, trailing whitespace and invisible spaces visually, and additional use the # sign at the end of lines to mark lines that extend off-screen. 
autocmd filetype html,xml set listchars-=tab:>. " In these files tabs are fine
set wrap         "wrap lines
set linebreak    "Wrap lines at convenient points


" ================ Folds ============================
set foldmethod=indent   "fold based on indent
set foldnestmax=3       "deepest fold is 3 levels
set nofoldenable        "dont fold by default


" ================ Completion =======================
set wildmode=list:longest
set wildmenu                "enable ctrl-n and ctrl-p to scroll thru matches
set wildignore=*.o,*.obj,*~,*.swp,*.bak,*.pyc,*.class "stuff to ignore when tab completing
set wildignore+=*vim/backups*
set wildignore+=*sass-cache*
set wildignore+=*DS_Store*
set wildignore+=vendor/rails/**
set wildignore+=vendor/cache/**
set wildignore+=*.gem
set wildignore+=log/**
set wildignore+=tmp/**
set wildignore+=*.png,*.jpg,*.gif


" ================ Scrolling ========================
set scrolloff=8         "Start scrolling when we're 8 lines away from margins
set sidescrolloff=15
set sidescroll=1


" ================ Search ===========================
set incsearch       " Find the next match as we type the search
set hlsearch        " Highlight searches by default
set ignorecase      " Ignore case when searching...
set smartcase       " ...unless we type a capital



" ================ Powerline-status =================
"python3 from powerline.vim import setup as powerline_setup
"python3 powerline_setup()
"python3 del powerline_setup
set rtp+=/usr/local/lib/python3.5/site-packages/powerline/bindings/vim
set laststatus=2        " Always show status line
set t_Co=256            " Use 256 colours (Use this settings only if the terminal supports 256 colors)

" ================ Markdown =========================
let g:vim_markdown_math=1               " Enable LaTeX math (e.g. $x^2$)
let g:vim_markdown_frontmatter=1        " Highlight YAML frontmatter
let g:vim_markdown_folding_disabled=0   " Folding is enabled
map asdf <Plug>Markdown_MoveToParentHeader " To move to parent header


" ================ Vim-Tmux-Navigator ===============
nnoremap <c-j> <c-w>j
nnoremap <c-k> <c-w>k
nnoremap <c-h> <c-w>h
nnoremap <c-l> <c-w>l

" ================ Vim-Cellmode Settings ============
nmap <silent> <Leader>F :call RunTmuxPythonChunk()<CR>
noremap <silent> <Leader>l :call RunTmuxPythonCell(0)<CR>
noremap <silent> <Leader>c :call RunTmuxPythonCell(1)<CR>
let g:cellmode_tmux_sessionname=''  " Will try to automatically pickup tmux session
let g:cellmode_tmux_windowname=''
let g:cellmode_tmux_panenumber='2'
let g:cellmode_n_files=100
let g:cellmode_use_tmux=1

" ================ Tslime.vim Settings ==============
let g:tslime_always_current_session = 1   " Use the current session
let g:tslime_always_current_window = 1    " Use the current window

vmap <silent> <Leader>r <Plug>SendSelectionToTmux    " Send a selection in visual mode to Tmux
nmap <silent> <Leader>r <Plug>NormalModeSendToTmux   " Grab the current method that a cursor is in normal mode, send to Tmux
nmap <Leader>x <Plug>SetTmuxVars            " To reset the session, window and pane info

" Have a command you run frequently, use this
" nmap <your_key_combo> : Tmux <Your command><CR>
" More info about the <Plug> and other mapping syntax can be found [here](http://vim.wikia.com/wiki/Mapping_keys_in_Vim_-_Tutorial_(Part_3\) ).

" ================ Vim-Screen Settings ===============


" ================ Vim.Slime Settings ===============
"let g:slime_target = "tmux"
"let g:slime_dont_ask_default = 1
"let g:slime_default_config = {'socket_name': 'default', 'target_pane': ':.2'}

"let g:slime_no_mappings = 1
"xmap <Leader>r <Plug>SlimeRegionSend
"nmap <Leader>r <Plug>SlimeParagraphSend
"nmap <Leader>v <Plug>SlimeConfig
"let g:slime_python_ipython = 1

" ================ Vimux Settings ===================
let g:VimuxUseNearest = 1       " Use the nearest pane in Tmux
let g:VimuxHeight = "50"        " The percentage of Vimux pane
let g:VimuxOrientation = "h"    " Split pane vertically

" Execurte the current file in Python3
" map <silent> <Leader>v :update<Bar>:call VimuxRunCommandInDir('if [[ $(pwd) =~ .*FincLab.* ]]; then; cwd=$(pwd); cd ~/Work/FincLab; else; cwd="."; fi; python3 "$cwd"/' . bufname("%"), 0)<CR>
map <silent> <Leader>v :update<Bar>:call VimuxRunCommandInDir('python3 "$(pwd)"/' . bufname("%"), 0)<CR>
map <silent> <Leader>f :update<Bar>:call VimuxRunCommandInDir('/cygdrive/c/Users/leepeteb/Downloads/Work/Bananajet/Anaconda/python.exe c:/users/leepeteb/Downloads/Work/BananaJet/' . bufname("%"), 0)<CR>
" map <silent> <Leader>v :update<Bar>:call VimuxRunCommandInDir('/cygdrive/c/Users/leepeteb/Downloads/Work/Bananajet/Anaconda/python.exe c:/users/leepeteb/Downloads/Work/BananaJet/lib/' . bufname("%"), 0)<CR>

map <silent> <Leader>sr :update<Bar>:call VimuxRunCommandInDir('sql @c:/users/leepeteb/Work/QMO/tests/' . bufname("%"), 0)<CR>

" map <Leader>ss :call VimuxRunCommand('set pagesize 50000; set feedback off; set markup html on; set spool on; set num 24; spool output_temp.xls; @h:\data\work\QMO\tests\' . bufname("%"))<CR>
map <Leader>ss :w<CR>:call VimuxRunCommand('set pagesize 50000;') <bar> call VimuxRunCommand('set feedback off;') <bar> call VimuxRunCommand('set termout on') <bar> call VimuxRunCommand('set define off;') <bar> call VimuxRunCommand('set markup html on spool on;') <bar> call VimuxRunCommand('set num 24;') <bar> call VimuxRunCommand('spool c:\users\leepeteb\temp\output_temp.xls;') <bar> call VimuxRunCommand('@h:\data\work\QMO\tests\' . bufname("%")) <bar> call VimuxRunCommand('set markup html off;') <bar> call VimuxRunCommand('spool off;') <bar> call VimuxRunCommand('set termout on') <CR>

" map <Leader>ss :call VimuxRunCommand('@h:\data\work\QMO\tests\' . bufname("%"))<CR>

map <silent> <Leader>m :update<Bar>:call VimuxRunCommandInDir('cd ~/Work/FincLab; python3 main.py', 0)<CR>

" Execute the current file using Bash
map <silent> <Leader>z :update<Bar>:call VimuxRunCommandInDir("bash " . bufname("%"), 0)<CR>
" Process: 1) Write the current buffer to its file. 2) Run the tmux command inside the current Vim file directory.

" ================ Python IDE =================
au FileType python map <silent> <leader>e oimport pdb; pdb.set_trace()<esc>
au FileType python map <silent> <leader>E Oimport pdb; pdb.set_trace()<esc>


" ================ NERDTree Settings ================
" Bind key for NerdTree
map <silent> <leader>ft :NERDTreeToggle<CR> 
let g:NERDTreeQuitOnOpen=1      " Quit NerdTree after openning a file


" ================ Rainbow Parentheses ==============
" nmap <Leader>( :RainbowParenthesesToggle<CR>
" au Syntax * RainbowParenthesesLoadRound
" au Syntax * RainbowParenthesesLoadSquare
" au Syntax * RainbowParenthesesLoadBraces

" ================ Jedi-Vim == ======================
let g:jedi#auto_initialization = 1            " Initialises with default key bindings
let g:jedi#completions_command = "<Ctrl-Space>"     " Performs autocompletion  Note: Compatible with Supertab to use <Tab> for completion
let g:jedi#goto_command = "<Leader>d"         " Go to definition
let g:jedi#goto_assignments_command = "<Leader>g"  " Finds the first definition of the function/class under the cursor. Produces an error if the definition is not in a python file.
let g:jedi#documentation_command = "<K>"      " Show pydoc documentation
let g:jedi#max_doc_height = 30                " Max height of documentation
let g:jedi#rename_command = "<Leader>c"       " Change the name of variables/functions.
let g:jedi#usages_command = "<Leader>n"       " Show usages of a name

let g:jedi#popup_on_dot = 1                   " Start completion on typing a period. May slow down performance.
let g:jedi#popup_select_first = 1             " Automatically select the first completion entry after hitting <Enter>
let g:jedi#auto_close_doc = 1                 " Auto close preview window upon leaving insert mode, when doing completion.
let g:jedi#show_call_signatures = 1           " Jedi-vim can display a small window detailing the arguments of the currently completed function and highlighting the currently selected argument. 0: Disabled; 2: Shows call signatures in the command line instead of a popup window.
let g:jedi#show_call_signatures_delay = 200  " Delay in milliseconds
let g:jedi#force_py_version = 3               " Default 'auto' will use sys.version_info from 'python' in your $PATH.
let g:jedi#smart_auto_mapping = 1             " When you start typing 'from module.name<space>' jedi-vim automatically adds the 'import' statement and displays the autocomplete popup.
let g:jedi#use_tag_stack = 1                  " To enable full tagstack functionality. It allows returning to the usage of a function with <CTRL-T> after exploring the definition with arbitrary changes to the jumplist.


" ================ Syntastic Settings ================
" set statusline+=%#warningmsg#
" set statusline+=%{SyntasticStatuslineFlag()}
" set statusline+=%*

" let g:syntastic_always_populate_loc_list = 1
" let g:syntastic_auto_loc_list = 1
" let g:syntastic_check_on_open = 1
" let g:syntastic_check_on_wq = 0
"
" let g:syntastic_python_checkers=["flake8"]
" let g:syntastic_python_flake8_args="--ignore=E501,W601,W391,F401"

" ================ Python-Mode ======================
" Checkers - Lint
let g:pymode_lint = 1           " Turn on code checking
let g:pymode_lint_on_write = 1  " Check code on every save
let g:pymode_lint_on_fly = 0    " Check code when editing on the fly
let g:pymode_lint_message = 1   " Show error message if cursor placed at the error line"
let g:pymode_lint_checkers = ['pyflakes', 'pep8', 'mccabe'] 
let g:pymode_lint_ignore = "E501,W" " Skip all warnings that start with E501

let g:pymode_lint_todo_symbol = 'WW'
let g:pymode_lint_comment_symbol = 'CC'
let g:pymode_lint_visual_symbol = 'RR'
let g:pymode_lint_error_symbol = 'EE'
let g:pymode_lint_info_symbol = 'II'
let g:pymode_lint_pyflakes_symbol = 'FF'
" let g:pymode_lint_options_pep8 = {'max_line_length': g:pymode_options_max_line_length} " PEP8 options
let g:pymode_lint_options_pyflakes = { 'builtins': '_' } " Pyflakes options
let g:pymode_lint_options_mccabe = { 'complexity': 20 } " mccabe options
let g:pymode_lint_options_pep257 = {} " pep257 options
"  let g:pymode_lint_options_pylint = {'max-line-length': g:pymode_options_max_line_length} " pylint options

" 4.Syntax
let g:pymode_syntax = 1         " Turn on pymode syntax
let g:pymode_syntax_slow_sync = 1       " Dsiabling this on slower hardware
let g:pymode_syntax_all = 1             " Enable all python highlights
let g:pymode_syntax_print_as_function = 0 " Highlight 'print' as a function
let g:pymode_syntax_highlight_equal_operator = g:pymode_syntax_all      " highlight '=' operator
let g:pymode_syntax_highlight_stars_operator = g:pymode_syntax_all      " highlight '*' operator
let g:pymode_syntax_highlight_self = g:pymode_syntax_all        " Highlight 'self' keyword
let g:pymode_syntax_indent_errors = g:pymode_syntax_all         " Highlight indent's errors
let g:pymode_syntax_space_errors = g:pymode_syntax_all          " Highlight space's errors
" Highlight string formatting
let g:pymode_syntax_string_formatting = g:pymode_syntax_all
let g:pymode_syntax_string_format = g:pymode_syntax_all
let g:pymode_syntax_string_templates = g:pymode_syntax_all
let g:pymode_syntax_doctests = g:pymode_syntax_all
let g:pymode_syntax_builtin_objs = g:pymode_syntax_all          " Highlight built-in objects
let g:pymode_syntax_builtin_types = g:pymode_syntax_all         " Highlight built-in types
let g:pymode_syntax_highlight_exceptions = g:pymode_syntax_all  " Highlight exceptions
let g:pymode_syntax_docstrings = g:pymode_syntax_all            " Highlight docstrings as pythonDocstring (otherwise as pythonString)

" ================ VimWordpress ===================
" map <silent> <Leader>bl :BlogList<CR>
" map <silent> <Leader>bn :BlogNew<CR>
" map <silent> <Leader>bs :BlogSave<CR>
" map <silent> <Leader>bp :BlogPreview<CR>
" map <silent> <Leader>bu :BlogUpload<CR>
" map <silent> <Leader>bo :BlogOpen<CR>
" map <silent> <Leader>bh :BlogSwitch<CR>
" map <silent> <Leader>bc :BlogCode<CR>

" ================ Other Settings ===================
" Remove trailing whitespace when Vim writes
function! TrimWhiteSpace()
    %s/\s\+$//e
endfunction
autocmd FileType python,java autocmd FileWritePre    * :call TrimWhiteSpace()
autocmd FileType python,java autocmd FileAppendPre   * :call TrimWhiteSpace()
autocmd FileType python,java autocmd FilterWritePre  * :call TrimWhiteSpace()
autocmd FileType python,java autocmd BufWritePre     * :call TrimWhiteSpace()






" User specified functions
" Strip all trailing whitespace in the current file
nnoremap <leader>W :%s/\s\+$//<cr>:let @/=''<CR>

" To re-hardwrap paragraphs of text
nnoremap <leader>q gqip

" Auto reload .vimrc upon changes
augroup myvimrc
    au!
    au BufWritePost .vimrc,_vimrc,vimrc,.gvimrc,_gvimrc,gvimrc so $MYVIMRC | if has('gui_running') | so $MYGVIMRC | endif
augroup END

" Quickly open/reload vim
nnoremap <leader>ev :split $MYVIMRC<CR>  
nnoremap <leader>sv :source $MYVIMRC<CR>   
